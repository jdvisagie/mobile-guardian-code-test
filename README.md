### Install Dependencies

I have two kinds of dependencies in this project: npm and npm json-server.

To get started, please run the following commands in your command line. 
```
sudo npm install
cd path/to/app/
sudo npm start
```

Then start up the json server and point it to your json file.

Please runs the following in your command line.
```
npm install -g json-server
cd path/to/app/assets/
json-server --watch db.json

```

You should be able to view the REST API in your browser here then: http://localhost:3000/

Your app should now be available http://localhost:8888/app/

Any issues, please shout and I'll be happy to help.