'use strict';

angular.module('myApp.friend_view', ['ngRoute', 'ngTagsInput']).config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/friend_view/:personID', {
		templateUrl : 'friend_view/friend_view.html',
		controller : 'FriendViewCtrl',
		resolve : {
			person : function(services, $route) {
				var personID = $route.current.params.personID;
				return services.getPerson(personID);
			}
		}
	});

}]).controller('FriendViewCtrl', ['$scope', '$rootScope', '$location', '$routeParams', 'services', 'person', '$route', function($scope, $rootScope, $location, $routeParams, services, person, $route) {
	$scope.person = person.data[0];
	$scope.skills = $scope.person.skills;

	$scope.deletePerson = function(person) {
		if (confirm("Are you sure to delete this person") == true)
			services.deletePerson(person.id);
		$location.path('/');
	};

	$scope.savePerson = function(person) {
		services.updatePerson(person);
		//debugger;
		$location.path('/');

	};
}]);
