'use strict';

angular.module('myApp.create_friend', ['ngRoute']).config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/create_friend/0', {
		templateUrl : 'create_friend/create_friend.html',
		controller : 'CreateFriendCtrl'

	});
}]).controller('CreateFriendCtrl', ['$scope', '$rootScope', '$location', 'services', function($scope, $rootScope, $location, services) {

	$scope.savePerson = function(person) {
		services.insertPerson(person);
		$location.path('/');
	};
}]); 