'use strict';

angular.module('myApp.friend_list_view', ['ngRoute', 'ui.bootstrap']).config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'friend_list_view/friend_list_view.html',
		controller : 'FriendListViewCtrl'
	});
}]).controller('FriendListViewCtrl', ['$scope', 'services', '$location', '$route', function($scope, services, $location, $route) {

	function getList() {
		console.log("Get people function ran");
		services.getPeople().then(function(data) {
			$scope.people = data.data;
		});
	}
	getList();
	
	$scope.deletePerson = function(id) {
		if (confirm("Are you sure to delete this person") == true)
			services.deletePerson(id);
		getList();
	};

}]);

/* This is what I was using to try connect to API... Didn't work... Think it was a CORS issue..

 $http({
 method: 'GET',
 url: 'https://api.floatschedule.com/api/v1/',
 headers: {
 'Authorization': 'Bearer 325dc0a457449671ef723e8da9f235ee3d1b8407',
 'Content-Type': 'application/x-www-form-urlencoded',
 'Accept': 'application/json'
 }

 }).then(function successCallback(response) {
 $scope.names = response.records;
 // success
 }, function errorCallback(response) {
 // errors
 });

 */

